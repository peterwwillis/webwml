# Copyright (C) 2004 Free Software Foundation, Inc.
# Juraj Kubelka <Juraj.Kubelka@email.cz>, 2004
#
msgid ""
msgstr ""
"Project-Id-Version: security.cs.po 1.1\n"
"PO-Revision-Date: 2006-08-12 17:15+0200\n"
"Last-Translator: Juraj Kubelka <Juraj.Kubelka@email.cz>\n"
"Language-Team: debian-www <debian-www@lists.debian.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/security/dsa.rdf.in:16
msgid "Debian Security"
msgstr ""

#: ../../english/security/dsa.rdf.in:19
msgid "Debian Security Advisories"
msgstr ""

#: ../../english/security/faq.inc:6
msgid "Q"
msgstr ""

#: ../../english/security/index.include:17
msgid ""
"<a href=\\\"undated/\\\">undated</a> security advisories, included for "
"posterity"
msgstr ""

#: ../../english/security/make-ref-table.pl:81
msgid "Mitre CVE dictionary"
msgstr ""

#: ../../english/security/make-ref-table.pl:84
msgid "Securityfocus Bugtraq database"
msgstr ""

#: ../../english/security/make-ref-table.pl:88
msgid "CERT Advisories"
msgstr ""

#: ../../english/security/make-ref-table.pl:92
msgid "US-CERT vulnerabilities Notes"
msgstr ""

#: ../../english/template/debian/security.wml:11
msgid "Source:"
msgstr "Zdroj:"

#: ../../english/template/debian/security.wml:15
msgid "Architecture-independent component:"
msgstr "Část nezávislá na architektuře:"

#. don't translate `<get-var url />'
#: ../../english/template/debian/security.wml:22
msgid ""
"MD5 checksums of the listed files are available in the <a href=\"<get-var "
"url />\">original advisory</a>."
msgstr ""
"MD5 kontrolní součty (checksums) uvedených souborů jsou dostupné v&nbsp;<a "
"href=\"<get-var url />\">původní zprávě</a>."

#. don't translate `<get-var url />'
#: ../../english/template/debian/security.wml:30
msgid ""
"MD5 checksums of the listed files are available in the <a href=\"<get-var "
"url />\">revised advisory</a>."
msgstr ""
"MD5 kontrolní součty (checksums) uvedených souborů jsou dostupné v&nbsp;<a "
"href=\"<get-var url />\">přepracované zprávě</a>."

#: ../../english/template/debian/security.wml:42
msgid "Debian Security Advisory"
msgstr "Bezpečnostní zprávy Debianu"

#: ../../english/template/debian/security.wml:47
msgid "Date Reported"
msgstr "Datum oznámění"

#: ../../english/template/debian/security.wml:50
msgid "Affected Packages"
msgstr "Zasažené balíčky"

#: ../../english/template/debian/security.wml:72
msgid "Vulnerable"
msgstr "Zranitelný"

#: ../../english/template/debian/security.wml:75
msgid "Security database references"
msgstr "Odkazy na bezpečnostní databázi"

#: ../../english/template/debian/security.wml:78
msgid "More information"
msgstr "Více informací"

#: ../../english/template/debian/security.wml:84
msgid "Fixed in"
msgstr "Opraveno v"

#: ../../english/template/debian/securityreferences.wml:16
msgid "BugTraq ID"
msgstr "BugTraq ID"

#: ../../english/template/debian/securityreferences.wml:60
msgid "Bug"
msgstr "Chyba"

#: ../../english/template/debian/securityreferences.wml:76
msgid "In the Debian bugtracking system:"
msgstr "V Debianím systému sledování chyb:"

#: ../../english/template/debian/securityreferences.wml:79
msgid "In the Bugtraq database (at SecurityFocus):"
msgstr ""

#: ../../english/template/debian/securityreferences.wml:82
msgid "In Mitre's CVE dictionary:"
msgstr ""

#: ../../english/template/debian/securityreferences.wml:85
msgid "CERT's vulnerabilities, advisories and incident notes:"
msgstr ""

#: ../../english/template/debian/securityreferences.wml:88
msgid "No other external database security references currently available."
msgstr ""
